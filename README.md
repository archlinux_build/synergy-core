# synergy-core

Open source core of Synergy, the keyboard and mouse sharing tool.

## Official Site

https://symless.com/synergy

## Git Repo

https://github.com/symless/synergy-core

## Download

Download **synergy-core** archlinux package from here:

* [synergy-core-x86_64.pkg.tar.zst](https://gitlab.com/archlinux_build/synergy-core/-/jobs/artifacts/master/browse?job=run-build)

## Disclaimer

This is a personal project to build the archlinux package of synergy. Take a look in CMakeLists.txt.patch - no source code here. Only the build pipeline.