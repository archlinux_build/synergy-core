# Contributor: Evaggelos Balaskas <evaggelos . balaskas . gr>
# Maintainer:  Evaggelos Balaskas <evaggelos . balaskas . gr>

_pkgname=synergy
pkgname=${_pkgname}-core
pkgver=1.14.3.5
pkgrel=1
pkgdesc="Open source core of Synergy, the keyboard and mouse sharing tool"
url="https://symless.com/synergy"

arch=('x86_64')
license=('GPL2')

makedepends=('cmake' 'glibc' 'patch' 'make' 'pkgconf' 'gcc')
depends=('libx11' 'libxtst' 'qt5-base' 'qt5-tools' 'icu' 'gdk-pixbuf2' 'libnotify' 'libxkbfile' 'xorg-xrandr' 'pugixml' 'libxinerama')

source=(https://github.com/symless/${pkgname}/archive/${pkgver}-stable.tar.gz
        CMakeLists.txt.patch)

sha256sums=('9259fc8aa02610121932cd5887c4d878beb08b2f7190e834c0584d53b36f5f9b'
            '6b93d1ef629edf3d1a38ed7fe9d246362e1624552225bd617ff0fa999ec9b8ee')

prepare() {
    cd $pkgname-${pkgver}-stable/
    export -p SYNERGY_NO_TESTS=yes
    patch -Np0  < ${srcdir}/CMakeLists.txt.patch
    mkdir -p ./build/
}

build() {
    cd $pkgname-${pkgver}-stable/build
    cmake -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX=/usr ..
    make
}

package() {
    cd $pkgname-${pkgver}-stable/build
    make install DESTDIR=${pkgdir}

    # Install config files
    install -Dm 644 ../doc/${_pkgname}.conf* -t "${pkgdir}/etc"

    # Install man files
    install -Dm 644 ../doc/${_pkgname}c.man "${pkgdir}/usr/share/man/man1/${pkgname}c.1"
    install -Dm 644 ../doc/${_pkgname}s.man "${pkgdir}/usr/share/man/man1/${pkgname}s.1"
}

# vim: tabstop=4 expandtab shiftwidth=4 softtabstop=4
